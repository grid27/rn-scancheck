set PROJECT_NAME=scancheck
set COMPANY=nevada
set DEVICE_ID=7LHUF6U899999999
REM set DEVICE_ID=10.21.8.134:5555
REM set DEVICE_ID=emulator-5554
REM set DEVICE_ID=MQH4C16A22000599

yarn bundle:android && cd .\android && gradlew assembleDebug && cd ..\ && adb -s "%DEVICE_ID%" install -r "./android/app/build/outputs/apk/debug/app-debug.apk" && adb -s "%DEVICE_ID%" shell am force-stop com.%COMPANY%.%PROJECT_NAME% && adb -s "%DEVICE_ID%" shell am start -n com.%COMPANY%.%PROJECT_NAME%/com.%COMPANY%.%PROJECT_NAME%.MainActivity
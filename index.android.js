import React, { Component } from 'react';
import { Provider } from "react-redux";
import { AppRegistry } from 'react-native';
import store from "./src/store";
import App from './src/App';

export default class ScannerChecks extends Component {
    render() {
      return (
        <Provider store={store}>
          <App />
        </Provider>
      )
    }
  }

AppRegistry.registerComponent('scancheck', () => ScannerChecks);
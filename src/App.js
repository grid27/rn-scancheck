/**
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import { StackNavigator } from 'react-navigation';
import { updateFocus } from 'react-navigation-is-focused-hoc';
import SplashScreen from 'react-native-splash-screen';

import Scanner from './components/Scanner';
import History from './components/History';
import Check from './components/Check';
import Settings from './components/Settings';
import ManualInput from './components/ManualInput';
import SettingsService from './services/settings.service';


let routes = {
  Scanner: { screen: Scanner },
  Settings: { screen: Settings },
  History: { screen: History },
  Check: { screen: Check },
  ManualInput: { screen: ManualInput },
}

let AppNavigator = null;

export default class AppContainer extends Component {

  state = {
    isLoaded: false
  }

  componentWillMount() {
    const settingsService = new SettingsService();
    settingsService.getSettings().then(({ firstScreen }) => {
      AppNavigator = StackNavigator(routes, {initialRouteName: firstScreen});
      this.setState({isLoaded: true});
      SplashScreen.hide();
    });
  }

  render() {
    if (this.state.isLoaded) {
      return (
        <AppNavigator
          onNavigationStateChange={(prevState, currentState) => {
            updateFocus(currentState);
          }}
        />
      )
    }
    return null;
  }
}
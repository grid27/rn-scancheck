import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
  Text,
  View,
  Alert,
  StatusBar,
  Dimensions,
  Platform,
  StyleSheet,
  Animated,
  Easing,
} from 'react-native';

import BarcodeScanner, { TorchMode , CameraFillMode, BarcodeType, FocusMode } from 'react-native-barcode-scanner-google';

import { Button, Icon } from 'native-base';
import RCTCamera from 'react-native-camera';
import { colors, text } from '../constants';
import Orientation from 'react-native-orientation';


const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
  },
  camera: {
    flex: 1,
  },
  capture: {
    position: 'absolute',
    flex: 0,
    borderRadius: 75/2,
    height: 75,
    width: 75,
    backgroundColor: colors.RED,
    bottom: 50,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  }, 
  rectangle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: 'rgba(0,0,0, .6)',
  },
  torchButton: {
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    top: 20, 
    right: 20, 
    backgroundColor: colors.DANGER,
    width: 50,
    height: 50,
    zIndex: 1
  },
  scannerHelpText: {
    position: 'absolute', 
    bottom: 0, 
    textAlign: 'center', 
    color: colors.LIGHT,
    padding: 20
  },
});

export default class Camera extends Component {

  state = {
    torchMode: false,
  }

  static propTypes = {
    barcodeScannerEnabled: PropTypes.bool,
    scannerHelpText: PropTypes.string,
    onBarCodeRead: PropTypes.func,
    scannerHeight: PropTypes.number,
    scannerWidth: PropTypes.number,
  }

  static defaultProps = {
    barcodeScannerEnabled: false,
    scannerHelpText: text.CAMERA_SCANNER_HELP_TEXT
  }

  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);
    if(this.props.barcodeScannerEnabled) {
      this.animatedValue = new Animated.Value(0);
      this.opacity = this.animatedValue.interpolate({
        inputRange: [0, 0.5, 1],
        outputRange: [0, 1, 0]
      });
    }
  }

  componentDidMount() {
    this.orientationListener = this._orientationDidChange.bind(this);
    Orientation.addOrientationListener(this.orientationListener);
    if (this.props.barcodeScannerEnabled) {
      this.animate();
    }
  }

  componentWillUnmount() {
    Orientation.removeOrientationListener(this.orientationListener);
  }

  _torchSwitch() {
    this.setState({torchMode: !this.state.torchMode})
  }

  _orientationDidChange(orientation) {
    setTimeout(() => {
      this.forceUpdate();
    }, 500);
    this.orientation = orientation;
  }

  animate() {
    this.animatedValue.setValue(0);
    Animated.timing(
      this.animatedValue,
      {
        toValue: 1,
        duration: 1000,
        easing: Easing.linear
      }
    ).start(() => this.animate());
  }

  capture() {
    if(!this.props.barcodeScannerEnabled) {
      return (
        <Button style={[styles.capture, {left: Dimensions.get('window').width / 2 - 75 / 2}]} onPress={this.takePicture.bind(this)}>
          <Icon name={"camera"} />
        </Button>
      )
    }
    else {
      let { height: winHeight, width: winWidth } = Dimensions.get('window');
      let height = _.max([winHeight * .5, 350]);
      let width = winWidth * .8;
      let borderHeight = (winHeight - height) / 2;
      let borderWidth = (winWidth - width) / 2;
      let rectangle = {
        borderLeftWidth: borderWidth,
        borderRightWidth: borderWidth,
        borderTopWidth: borderHeight,
        borderBottomWidth: borderHeight,
      }
      return (
        <View style={{flex: 1}}>
          <Button rounded style={styles.torchButton} onPress={this._torchSwitch.bind(this)}>
            <Icon style={{color: this.state.torchMode? colors.WARNING: colors.WHITE}} name={"flash"} />
          </Button>
          <View style={[rectangle, styles.rectangle]}>
            <View style={{
              height,
              width,
              justifyContent: 'center',
            }}>
              <Animated.View style={{opacity: this.opacity, width: width, height: 1, backgroundColor: colors.RED}}/>
            </View>
          </View>
          <Text style={styles.scannerHelpText}>{this.props.scannerHelpText}</Text>
        </View>
      )
    }
  }

  _renderCamera() {
    // Include fast barcodereader if android < 7.0
    if (Platform.OS === 'android' && Platform.Version < 24 && this.props.barcodeScannerEnabled) {
      // Fixed size at viewfinder BarcodeScanner
      let width ='110%', height = '110%';
      if (this.orientation === 'LANDSCAPE') {
        width = '110%', height = '210%';
      }
      return(
        <View style={{flex: 1}}>
          <BarcodeScanner
            style={{flex:1, position: 'absolute', width, height}}
            onBarCodeRead={this.props.onBarCodeRead}
            onException={(e) => console.error(e)}
            focusMode={FocusMode.TAP}
            // barcodeType={BarcodeType.QR_CODE}
            torchMode={this.state.torchMode? TorchMode.ON: TorchMode.OFF}
            cameraFillMode={CameraFillMode.COVER}
          />
          {this.capture()}
        </View>
      )
    }
    else {
      return (
        <RCTCamera
          style={ styles.camera }
          onBarCodeRead={ this.props.onBarCodeRead }
          barcodeScannerEnabled={ this.props.barcodeScannerEnabled }
          // barCodeTypes={RCTCamera.constants.BarCodeType.qr}
          // clearWindowBackground={true}
          torchMode={
            this.state.torchMode? RCTCamera.constants.TorchMode.on: RCTCamera.constants.TorchMode.off
          }
          aspect={ RCTCamera.constants.Aspect.fill }>
          {this.capture()}
        </RCTCamera>
      )
    }
  }

  render() {
    return (
      <View style={styles.container}>
        {this._renderCamera()}
      </View>
    );
  }

  takePicture() {
    const options = {};
    this.camera.capture({metadata: options})
      .then((data) => {
        // TODO Сделать вывод фотографии
        // actionsApp.addPhoto(data.path);
      })
      .catch(err => console.error(err));
  }
}
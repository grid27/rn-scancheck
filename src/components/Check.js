import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, StatusBar, StyleSheet } from 'react-native';
import {
  Container, Content, Text, Button,
  Header, Left, Icon, Body, Title,
  List, ListItem, H1, H2
} from 'native-base';

import moment from 'moment';
import SimpleToast from 'react-native-simple-toast';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';

import { colors, text } from '../constants';


// const styles = StyleSheet.create({
  
// });

export default class Check extends Component { 

  static navigationOptions = {
    header: null,
  }

  _renderHeader (props) {
    const { goBack } = this.props.navigation;
    return (
      <Header style={{backgroundColor: colors.PRIMARY}}>
        <Left>
          <Button transparent onPress={() => goBack()}>
            <Icon style={{color:colors.WHITE}} name='arrow-back' />
          </Button>
        </Left>
        <Body>
          <Title>Чек</Title>
        </Body>
      </Header>
    )
  }

  render() {
    let {navigate, state: {params: { check }}} = this.props.navigation;
    return (
      <Container>
        {this._renderHeader()}
        <Content>
          <View style={{paddingHorizontal: 15}}>
            <Text>{moment(check.date_time).format('DD.MM.YYYY HH:MM')}</Text>
            <H1>{check.rival || text.CHECK_NO_NAME}</H1>
            <H2>{check.total_sum} руб.</H2>
          </View>
          <List>
            {
              check.wares.map((ware, index) => {
                return (
                  <ListItem key={index}>
                    <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between'}}>
                      <Text numberOfLines={1} style={{fontSize: 12, width: '55%'}}>{ware.name}</Text>
                      <Text style={{fontSize: 12}}>
                        {ware.quantity} * {ware.price} = {(parseFloat(ware.quantity) * parseFloat(ware.price)).toFixed(2)}
                      </Text>
                    </View>
                  </ListItem>
                )
              })
            }
          </List>
        </Content>
      </Container>
    )
  }
}
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, StatusBar, TouchableOpacity, Alert } from 'react-native';
import {
  Container, Content, Text, Button,
  Header, Left, Icon, Body, Title,
  List, ListItem, SwipeRow
} from 'native-base';

import moment from 'moment';
import SimpleToast from 'react-native-simple-toast';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';

import { colors, text } from '../constants';
import OFDService from '../services/ofd.service';


class HistoryContainer extends Component {

  ofdService = new OFDService();   

  state = {
    openModal: false
  }

  rows = {};

  static navigationOptions = {
    header: null,
  }

  _renderHeader (props) {
    return (
      <Header style={{backgroundColor: colors.PRIMARY}}>
        <Left>
          <Button transparent onPress={() => this.props.navigation.navigate('Scanner')} iconLeft>
            <Icon style={{color:colors.WHITE}} name='arrow-back'/>
            <Text style={{fontSize: 14, paddingTop: 5}}>Сканер</Text>
          </Button>
        </Left>
        <Body>
          <Title style={{textAlign: 'right', width: '100%'}}>История</Title>
        </Body>
      </Header>
    )
  }

  _removeCheckAlert(check) {
    Alert.alert(
      'Подтвердите действие',
      `Удалить чек ${check.rival}?`,
      [
        {text: 'Отмена', style: 'cancel'},
        {text: 'OK', onPress: () => {
          this.rows[this.lastIndexRow].closeRow();
          this.lastIndexRow = -1;
          this.ofdService.rmCheck(check);
        }},
      ],
      { cancelable: true }
    )
  } 

  render() {
    const { navigate } = this.props.navigation;
    return (
      <Container>
        {this._renderHeader()}
        <Content>
          <List>
            {this.props.checks.map((check, index) => {
              return (
                <SwipeRow
                  key={index}
                  rightOpenValue={-75}
                  closeOnRowPress={true}
                  disableRightSwipe={true}
                  ref={ref => this.rows[index] = ref? ref.wrappedInstance: null}
                  onRowOpen={() => {
                    if (this.lastIndexRow > -1) {
                      this.rows[this.lastIndexRow].closeRow();
                    }
                    this.lastIndexRow = this.lastIndexRow !== index? index: -1;
                  }}
                  body={
                    <TouchableOpacity
                      style={{width: '100%'}}
                      delayLongPress={1000}
                      onPress={() => navigate('Check', { check })}
                    >
                      <View style={{width: '100%', paddingHorizontal: 8}}>
                        <Text style={{fontSize: 14, color: colors.GRAY}}>
                          {moment(check.date_time).format('DD.MM.YYYY HH:MM')}
                        </Text>
                        <Text>{check.rival || text.CHECK_NO_NAME} | {check.total_sum} руб.</Text>
                      </View>
                    </TouchableOpacity>
                  }
                  right={
                    <Button danger onPress={() => this._removeCheckAlert(check)}>
                      <Icon active name="trash" />
                    </Button>
                  }
                />
              )
            })}
          </List>
        </Content>
      </Container>
    )
  }
}

const mapStateToProps = (store) => {
  return {
    checks: store.appState.checks
  }
}

export default connect(mapStateToProps)(HistoryContainer);
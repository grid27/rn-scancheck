import _ from 'lodash'
import React, { Component } from 'react';

import { View, TouchableWithoutFeedback, Keyboard } from 'react-native';

import {
  Container, Content, 
  Text, Button,
  Header, Left, Icon, 
  Body, Title
} from 'native-base';

import { TextField } from 'react-native-material-textfield';
import SimpleToast from 'react-native-simple-toast';

import { colors, text } from '../constants';
import OFDService from '../services/ofd.service';
import Spinner from './Spinner';
import SettingsService from '../services/settings.service';


export default class ManualInput extends Component {

  ofdService = new OFDService();

  state = {
    fields: {
      'fn': {label: 'ФН', value: '', error: null},
      'fs': {label: 'ФП', value: '', error: null},
      'fd': {label: 'ФД', value: '', error: null},
    }
  }

  static navigationOptions = {
    header: null,
  }

  _getCheckInfo() {
    if (this._validateFields()) {
      let { fn: { value: fn }, fs: { value: fs }, fd: { value: fd } } = this.state.fields;
      Keyboard.dismiss();
      this.ofdService.getCheckInfo({fn, fs, fd})
      .then((check) => {
        this.props.navigation.navigate('Check', { check });
      })
      .catch((e) => {
        SimpleToast.show(e.message, 5000);
      });
    }
  }

  _validateFields() {
    return (
      Object.keys(this.state.fields).map(name => {
        return this._checkFields(name);
      }).indexOf(true) === -1
    );
  }

  _checkFields(name) {
    let fields = Object.assign({}, this.state.fields), isError = false;
    if (!fields[name].value) {
      fields[name].error = text.REQUIRED_FIELD;
      isError = true;
    }
    else if(fields[name].error) {
      fields[name].error = null;
    }
    this.setState({ fields });
    return isError;
  }

  _renderHeader (props) {
    const { goBack, pop } = this.props.navigation;
    console.log(this.props);
    return (
      <Header style={{backgroundColor: colors.PRIMARY}}>
        <Left>
          <Button transparent onPress={() => this.props.navigation.navigate('Scanner')} iconLeft>
            <Icon style={{color:colors.WHITE}} name='arrow-back'/>
            <Text style={{fontSize: 14, paddingTop: 5}}>Сканер</Text>
          </Button>
        </Left>
        <Body>
          <Title style={{textAlign: 'right', width: '100%'}}>Ручной ввод</Title>
        </Body>
      </Header>
    )
  }

  _renderFields() {
    return (
      Object.entries(this.state.fields).map(([name, {label, value, error}], index) => {
        return (
          <TextField
            key={index}
            label={label}
            value={value.toString()}
            keyboardType={'numeric'}
            onChangeText={(value) => {
              this.state.fields[name].value = value.replace(/[^\d]/g, '') || '';
            }}
            onBlur={() => this._checkFields(name)}
            error={error}
          />
        );
      })
    );
  }

  render() {
    const { navigate } = this.props.navigation;
    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
        <Container>
          <Spinner listenLoad={['getCheckInfo']} />
          {this._renderHeader()}
          <Content>
            <View style={{paddingHorizontal: 15}}>
              {this._renderFields()}
              <Button style={{width: '100%', marginTop: 30, backgroundColor: colors.SECONDARY}} onPress={this._getCheckInfo.bind(this)}>
                <Text style={{width: '100%', textAlign: 'center'}}>{text.SCANNER_FORM_BUTTON_NAME}</Text>
              </Button>
            </View>
          </Content>
        </Container>
      </TouchableWithoutFeedback>
    )
  }
}

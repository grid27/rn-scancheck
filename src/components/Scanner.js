import _ from 'lodash'
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, StatusBar, StyleSheet } from 'react-native';
import {
  Container, Content, Text, Button,
  Header, Icon, Body, Title, Left, Spinner as BaseSpinner,
  List, ListItem, Footer, FooterTab, Right
} from 'native-base';

import SimpleToast from 'react-native-simple-toast';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import { withNavigationFocus } from 'react-navigation-is-focused-hoc'

import { colors, text } from '../constants';
import Camera from './Camera';
import Spinner from './Spinner';
import OFDService from '../services/ofd.service';
import SetttingsService from '../services/settings.service';


const styles = StyleSheet.create({
  
});


class ScannerContainer extends Component {

  ofdService = new OFDService();
  settingsService = new SetttingsService();

  state = {
    barcodeScannerEnabled: true,
    isFocused: true
  }

  static navigationOptions = {
    header: null,
  }

  navigate(routeName, params={}) {
    // given timeout, for smooth the transition animation
    if (!this.flagWaitingNavigate) {
      setTimeout(() => this.setState({isFocused: false}), 500);
      this.flagWaitingNavigate = true;
      this.props.navigation.navigate(routeName, params);
    }
  }

  componentWillReceiveProps(nextProps) {
    if (!this.props.isFocused && nextProps.isFocused) {
      // screen enter (refresh data, update ui ...)
      setTimeout(() => this.setState({isFocused: true}), 500);
      this.flagWaitingNavigate = false;
    }
    // if (this.props.isFocused && !nextProps.isFocused) {
    //   // screen exit
    //   this.setState({isFocused: false});
    // }
  }

  _renderHeader() {
    return (
      <Header noShadow style={{backgroundColor: colors.PRIMARY}}>
        <Left>
          <Button transparent onPress={() => this.navigate('Settings')}>
            <Icon style={{color:colors.WHITE}} name={'settings'}/>
          </Button>
        </Left>
        <Body>
          <Title>Сканер чеков</Title>
        </Body>
        <Right>
          <Button transparent onPress={() => this.navigate('History')} iconRight >
            <Text style={{fontSize: 14, paddingTop: 5}}>История</Text>
            <Icon style={{color: colors.WHITE}} name={'arrow-forward'}/>
          </Button>
        </Right>
      </Header>
    )
  }

  onBarCodeRead({ data, type }) {
    if (this.flagScann || !~['QR_CODE'].indexOf(type)) return false;
    this.flagScann = true;
    this.ofdService.getCheckInfo(data)
    .then(check => {
      setTimeout(() => this.flagScann = false, 1000);
      if (this.settingsService.streamScanning) {
        SimpleToast.showWithGravity(text.SCANNER_STREAM_TOAST, 5000, SimpleToast.CENTER);
      }
      else {
        this.navigate('Check', { check });
      }
    })
    .catch((e) => {
      setTimeout(() => this.flagScann = false, 1000);
      SimpleToast.showWithGravity(e.message, 3000, SimpleToast.CENTER);
    });
    
  }

  _renderCamera() {
    // if the component is out of focus, then disabled camera
    if(this.state.isFocused) {
      return (
        <Camera
          onBarCodeRead={this.onBarCodeRead.bind(this)}
          barcodeScannerEnabled={this.state.barcodeScannerEnabled}
        />
      )
    }
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <BaseSpinner animating={true} size={20} />
      </View>
    )
  }

  render () {
    return (
      <Container>
        <Spinner backgroundColor={'rgba(0,0,0,.8)'} listenLoad={['getCheckInfo']} />
        {this._renderHeader()}
        {this._renderCamera()}
        <Footer>
          <FooterTab>
            <Button 
              onPress={() => this.navigate('ManualInput')}
              style={{backgroundColor: colors.BROWN}}
            >
              <Text style={{color: colors.WHITE}}>
                Ввести вручную
              </Text>
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    )
  }
}

const mapStateToProps = (store) => {
    return {
      checks: store.appState.checks
    }
}

export default withNavigationFocus(connect(mapStateToProps)(ScannerContainer));

import _ from 'lodash'
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { 
  View, StatusBar, StyleSheet, 
  TouchableOpacity, Alert, Switch
} from 'react-native';
import {
  Container, Content, Text, Button,
  Header, Left, Icon, Body, Title,
  List, ListItem, SwipeRow
} from 'native-base';

import { TextField } from 'react-native-material-textfield';
import { Dropdown } from 'react-native-material-dropdown';
import moment from 'moment';
import SimpleToast from 'react-native-simple-toast';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';

import { colors, text } from '../constants';
import SetttingsService from '../services/settings.service';


const styles = StyleSheet.create({
  saveButton: {
    width: '100%',
    marginTop: 30,
  },
  switch: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 15
  }
});


export default class Settings extends Component {

  settingsService = new SetttingsService();

  state = {
    fields: {
      maxCheck: { 
        value: 100, 
        label: text.SETTINGS_LABEL_MAX_CHECK, 
        error: null,
        keyboardType: 'numeric',
        type: 'input',
        changed: false,
        maxSize: 3,
      },
      firstScreen: { 
        value: 'Scanner', 
        label: text.SETTINGS_LABEL_FIRST_SCREEN,
        type: 'select',
        options: [
          {label: 'Сканер', value: 'Scanner'},
          {label: 'Ручной ввод', value: 'ManualInput'},
          {label: 'История сканирования', value: 'History'},
        ]
      },
      streamScanning: {
        label: text.SETTINGS_STREAM_SCANNING,
        value: false,
        type: 'switch',
        changed: false,
      }
    },
    oldSateField: {},
  };

  static navigationOptions = {
    header: null,
  };

  componentWillMount() {
    let fields = Object.assign({}, this.state.fields);
    let oldSateField = {};

    Object.keys(fields).forEach(name => {
      let value = this.settingsService[name];
      fields[name].value = value;
      oldSateField[name] = value;
    });
    this.setState({ fields, oldSateField });
  };

  _isChanged() {
    for (let field of Object.values(this.state.fields)) {
      if (field.changed) return true;
    }
    return false;
  };

  _isError() {
    return (
        Object.entries(this.state.fields).map(([name, { value }]) => {
          return this._checkFields(name, value);
      }).indexOf(true) > -1
    );
  };

  _saveSettings(screen) {
    if (!this._isError()) {
      let oldSateField = Object.assign(this.state.oldSateField);
      let fields =  Object.assign(this.state.fields);

      let settings = {};

      for (let [name, { value }] of Object.entries(fields)) {
        settings[name] = value;
        oldSateField[name] = value;
        fields[name].changed = false;
      }
      this.settingsService.save(settings);
      this.setState({ oldSateField, fields });
      SimpleToast.show(text.SETTINGS_SAVE_SUCCESS, 10000);
    }
  };

  _checkFields(name, value) {
    let fields = Object.assign({}, this.state.fields);
    switch (name) {
      case 'maxCheck':
        fields[name].value = parseInt(
          value.toString()
               .replace(/[^\d]/g, '')
               .slice(0, fields[name].maxSize) || 0
        );
        if (fields[name].value < 10 || fields[name].value > 100) {
          fields[name].error = text.SETTINGS_ERROR_MAX_CHECK;
        }
        else {
          fields[name].changed = this.state.oldSateField[name] != value;
          fields[name].error = null;
        }
        break;
      default:
        fields[name].changed = this.state.oldSateField[name] != value;
        fields[name].value = value;
    };
    this.setState({ fields });
    return Boolean(fields[name].error);
  };

  _renderHeader(props) {
    const { goBack } = this.props.navigation;
    return (
      <Header style={{backgroundColor: colors.PRIMARY}}>
        <Left>
          <Button transparent onPress={() => goBack()}>
            <Icon style={{color:colors.WHITE}} name='arrow-back' />
          </Button>
        </Left>
        <Body>
          <Title>Настройки приложения</Title>
        </Body>
      </Header>
    )
  };

  _renderFields() {
    return (
      Object.entries(this.state.fields).map(([name, field], index) => {
        if(field.type === 'input') {
          return (
            <TextField
              key={index}
              label={field.label}
              value={(field.value || '').toString()}
              keyboardType={field.keyboardType}
              onChangeText={value => this._checkFields(name, value)}
              error={field.error}
            />
          )
        }
        else if (field.type === 'select') {
          return (
            <Dropdown
              key={index}
              data={field.options}
              label={field.label}
              value={field.value}
              onChangeText={value => this._checkFields(name, value)}
            />
          )
        }
        else if (field.type === 'switch') {
          return (
            <View key={index} style={styles.switch}>
              <Text>{text.SETTINGS_STREAM_SCANNING}</Text>
              <Switch
                onValueChange={value => this._checkFields(name, value)}
                value={field.value}
              />
            </View>
          )
        }
      })
    );
  };

  render() {
    const { navigate } = this.props.navigation;
    const isChanged = this._isChanged();
    return (
      <Container>
        {this._renderHeader()}
        <Content>
          <View style={{paddingHorizontal: 15}}>
            {this._renderFields()}
            <Button
              disabled={!isChanged} 
              style={[styles.saveButton, {backgroundColor: !isChanged? colors.GRAY: colors.SECONDARY}]} 
              onPress={this._saveSettings.bind(this)}
            >
              <Text style={{width: '100%', textAlign: 'center'}}>{text.SAVE_BUTTON}</Text>
            </Button>
          </View>
        </Content>
      </Container>
    );
  };
}

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import BaseSpinner from 'react-native-spinkit';
import { StyleSheet, View } from 'react-native';
import RestService from "../services/rest.service";
import { colors } from '../constants';


const styles = {
  container: {
    ...StyleSheet.absoluteFillObject,
    zIndex: 999,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  }
}

class Spinner extends Component {

  unsubscribe = null;

  state = {
    listenLoad: null,
  }

  static defaultProps = {
    backgroundColor: 'rgba(255,255,255,0.6)',
    size: 50,
    // 'CircleFlip', 'Bounce', 'Wave', 'WanderingCubes', 'Pulse', 'ChasingDots', 
    // 'ThreeBounce', 'Circle', '9CubeGrid', 'WordPress', 
    // 'FadingCircle', 'FadingCircleAlt', 'Arc', 'ArcAlt'
    type: 'Wave' 
  }

  static propTypes = {
    listenLoad: PropTypes.arrayOf(PropTypes.string),
    size: PropTypes.number,
    backgroundColor: PropTypes.string,
    type: PropTypes.string,
  }

  get flagLoad() {
    return Object.values(this.state.listenLoad).indexOf(true) > -1;
  }

  constructor(props) {
    super(props);
    this.rest = new RestService();
    this.state.listenLoad = this.rest.getLoads(this.props.listenLoad);
  }

  componentDidMount() {
    this.unsubscribe = this.rest.subscribe((obj) => {
      Object.entries(obj).forEach(([key, status]) => {
        if(this.state.listenLoad[key] !== undefined) {
          let listenLoad = Object.assign({}, this.state.listenLoad, {[key]: status});
          this.setState({listenLoad});
        }
      });
    });
  }

  componentWillUnmount() {
    this.unsubscribe();
  }

  render() {
    if (this.flagLoad){
      return (
        <View style={[styles.container, {backgroundColor: this.props.backgroundColor}]}>
          <BaseSpinner type={this.props.type} color={colors.SECONDARY} size={this.props.size} />
        </View>
      );
    }
    return null
  }
}

export default Spinner;
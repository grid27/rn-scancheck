export const colors = {
  PRIMARY:     '#3F51B5',
  SECONDARY:   '#00c07b',
  DANGER:      '#f53d3d',
  LIGHT:       '#E9E9EF',
  DARK:        '#222222',
  WARNING:     '#eed531',
  STATUS_BAR:  '#324191',
  SHADOW:      '#DEDEDE',
  GRAY:        '#808080',
  WHITE:       '#FFFFFF',
  RED:         '#FE2F2F',
  BLACK:       '#000000',
  BROWN:       '#59534E',
}

export const types = {
  ADD_CHECKS:    0,
  ADD_CHECK:     1,
  RM_CHECK:      2,
}

export const text = {
  ERROR_TYPE_BARCODE:          'Неверный штрих-код',
  ERROR_GET_CHECK:             'Невозможно получить информацию для этого чека',
  SETTINGS_LABEL_MAX_CHECK:    'Максимальное количество чеков в истории',
  SETTINGS_ERROR_MAX_CHECK:    'Введите 10 и более, но не больше 100',
  SETTINGS_LABEL_FIRST_SCREEN: 'Первый экран',
  SETTINGS_SAVE_SUCCESS:       'Настройки сохранены',
  SETTINGS_STREAM_SCANNING:    'Потоковое сканирование',
  SCANNER_STREAM_TOAST:        'Готово, отсканируйте следующий чек',
  SAVE_BUTTON:                 'Сохранить',
  CAMERA_SCANNER_HELP_TEXT:    'Поместите штрих-код в прямоугольник видоискателя',
  CHECK_NO_NAME:               'Без названия',
  REQUIRED_FIELD:              'Обязательное поле',
  SCANNER_FORM_BUTTON_NAME:    'Получить чек',
}
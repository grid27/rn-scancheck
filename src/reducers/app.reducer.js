import { types } from '../constants';
import OFDService from '../services/ofd.service';
import SettingsService from '../services/settings.service';


const ofdService = new OFDService();
const settingsService = new SettingsService();

const initialState = {
  checks: [],
};

const appReducer = (state = initialState, action) => {
  let newState, checks, settings;
  switch (action.type) {
    case types.ADD_CHECK:
      checks = [action.check].concat(state.checks);
      checks = checks.slice(0, settingsService.maxCheck);
      newState = Object.assign({}, state, { checks });
      ofdService.saveCheckFromStorage(checks);
      break;
    case types.ADD_CHECKS:
      newState = Object.assign({}, state, { checks: action.checks });
      break;
    case types.RM_CHECK:
      checks = state.checks.filter(c => {return c.params !== action.check.params});
      newState = Object.assign({}, state, { checks });
      ofdService.saveCheckFromStorage(checks);
      break;
    default:
      newState = state;
  }
  return newState;
};

export default appReducer;
import RestService from "./rest.service";
import { AsyncStorage } from 'react-native';
import store from '../store';
import { types, text } from '../constants';


let instance = null;

export default class OFDService {

  _rest = new RestService();

  apiCheck = 'ofd/check-info';

  constructor() {
    if(!instance) {
      instance = this;
      this.getCheckFromStorage();
    }
    return instance;
  }

  async getCheckInfo(barcode) {
    
    let response;
    let { checks } = store.getState('appState').appState;
    let params = Object.values(barcode).join(';')
    let check = checks.find(x => {return x.params === params});

    if (!check) {
      if (typeof barcode === 'string') {
        barcode = this.parseBarcode(barcode);
      }

      let { fn, fd, fs } = barcode;

      if (!fn || !fd || !fs) {
        throw Error(text.ERROR_TYPE_BARCODE);
      }
      
      try {
        response = await this._rest.authGet(
          this._rest.getUrl(this.apiCheck),
          {
            fiscal_drive: fn,
            fiscal_document: fd,
            fiscal_sign: fs
          },
          {},
          { loadName: 'getCheckInfo', cacheTimelife: 0 }
        )
      }
      catch(e) {
        throw Error(text.ERROR_GET_CHECK);
      }

      if (response.data.error) {
        throw Error(response.data.message || text.ERROR_GET_CHECK);
      }

      check = response.data;
      check.params = params;
      this.addCheck(check);
    }
    return check;
  }

  parseBarcode(barcode) {
    let tmp = {};
    barcode.split('&').forEach(it => {
      let [k, v] = it.split('=');
      tmp[k] = v;
    });
    let { fn, i: fd, fp: fs } = tmp;
    return { fn, fd, fs };
  }

  addCheck(check) {
    store.dispatch({
      check,
      type: types.ADD_CHECK
    });
  }

  rmCheck(check) {
    store.dispatch({
      check,
      type: types.RM_CHECK
    });
  }

  saveCheckFromStorage(checks) {
    AsyncStorage.setItem('checks', JSON.stringify(checks)).catch(e => {
      console.warn("No save checks!");
    });
  }

  getCheckFromStorage() {
    AsyncStorage.getItem('checks').then(raw => {
      let checks = JSON.parse(raw);
      if (raw) {
        store.dispatch({
          checks,
          type: types.ADD_CHECKS
        });
      }
    });
  }
}
import axios from 'axios';
import { AsyncStorage } from 'react-native';
import { CACHE_TIME_LIFE, DKEY, SERVER_URL } from '../config';


let instance = null;

export default class RestService {

  _loads = {};
  counterLoads = 0;

  callbacks = [];

  _prefLoads = [];

  headersFakeBrowser = {
    "X-Requested-With": "XMLHttpRequest", 
    "Accept-Language": "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7",
    "Accept": "application/json, text/javascript, */*; q=0.01", 
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36"
  }

  constructor() {
    if(!instance) {
      instance = this;
      this.clearCache();
    }
    return instance;
  }

  subscribe(callback) {
    if(!~this.callbacks.indexOf(callback)) {
      this.callbacks.push(callback);
    }
    return () => this.callbacks = this.callbacks.filter(clb => { return clb !== callback });
  }

  async clearCache() {
    try {
      let keys = await AsyncStorage.getAllKeys();
      let cached = await AsyncStorage.multiGet(keys);
      let currentTime = Date.now();
      let rmCache = [];
      cached.forEach(([key, value]) => {
        let { timelife, time } = JSON.parse(value);
        if (timelife && time && (force || currentTime - time >= timelife)) {
          rmCache.push(key);
        }
      });
      if (rmCache.length) AsyncStorage.multiRemove(rmCache);
    }
    catch(e) {}
  }

  get authHeader() {
    return {
      'Authorization' : `Token ${this.token}`
    }
  }

  get loads() {
    return this._loads;
  }

  forceUpdate() {
    this.callbacks.forEach(clb => clb(arguments[0]));
  }

  updateLoad(loadName, status) {
    loadName = loadName.replace(/[0-9]*_/, '');
    let loadsName = Object.keys(this.loads).filter(key => {return key.indexOf(loadName) > -1});
    for (let _loadName of loadsName){
      if (this.loads[_loadName]){
        status = true;
        break;
      }
    }
    this.forceUpdate({[loadName]: status})
  }

  getLoads(names) {
    let loads = {};
    names.forEach(name => {
      loads[name] = false;
    });
    names.forEach(name => {
      this._prefLoads.forEach(_pref => {
        let _loadName = `${_pref}_${name}`;
        if (this.loads[_loadName]) {
          loads[name] = true;
        }
      });
    })
    return loads;
  }

  _getPrefLoad() {
    let _pref = Math.random().toString().slice(2);
    if (this._prefLoads.indexOf(_pref) > - 1) {
      return this._getPrefLoad();
    }
    this._prefLoads.push(_pref);
    return [_pref, ()=>this._prefLoads.filter(x=> x !== _pref)];
  }

  _addToLoads(loadName) {
    let [_pref, rmPref] = this._getPrefLoad();
    loadName = `${_pref}_${loadName}`;
    this._loads[loadName] = true;
    this.updateLoad(loadName, true);
    return () => this._rmLoadName(loadName, rmPref);
  }

  _rmLoadName(loadName, rmPref) {
    delete this._loads[loadName];
    rmPref();
    this.updateLoad(loadName, false);
  }

  async _saveCache(timelife, key, data) {
    try {
      await AsyncStorage.setItem(key, JSON.stringify({timelife, time: Date.now(), data}));
    }
    catch({code, message}) {
      console.log(`save cache error: ${key}, ${JSON.stringify(data)} `, message, code);
    }
    finally {
      return null;
    }
  }

  async _getCache(key) {
    try {
      console.log('_getCache');
      let cache = await AsyncStorage.getItem(key);
      if(cache){
        let  { timelife, time, data } = JSON.parse(cache);
        if ( Date.now() - time < timelife ){
          return data;
        }
        else {
          await AsyncStorage.removeItem(key);
          return null;
        }
      }
      else return null;
    }
    catch({code, message}) {
      console.warn(`get cache error: ${key} `, message, code);
      return null;
    }
  }

  async _cachingResponse(callbackResponse, hash, settings={}) {
    let { cacheTimelife, loadName } = settings, cache = null;
    cacheTimelife = cacheTimelife !== undefined? cacheTimelife: CACHE_TIME_LIFE;
    let rmLoadName = null;
    if (loadName) {
      rmLoadName = this._addToLoads(loadName);
    }
    this.counterLoads++;
    try {
      if (cacheTimelife) {
        cache = await this._getCache(hash);
      }
      if (!cacheTimelife || !cache) {
        let response = await callbackResponse();
        if(cacheTimelife) {
          this._saveCache(cacheTimelife, hash, response);
        }
        return response;
      }
      else return cache;
    }
    catch (e) {
      console.error(e);
      this._handlerError(e);
    }
    finally {
      if (rmLoadName) {
        rmLoadName();
      }
      this.counterLoads--;
    }
  }

  async get(url, params, headers, settings={}) {
    return await this._cachingResponse(
      () => axios.get(url, {params, headers}),
      this._getHashName(url + JSON.stringify(params), settings.cacheTimelife),
      settings
    )
  }

  async post(url, data, config, settings={}) {
    let fd = new FormData();
    for(let [k,v] of Object.entries(data)) { fd.append(k, v) };
    let hash = RestService.hashCode(url + JSON.stringify(config));
    return await this._cachingResponse(
      () => axios.post(url, fd, config),
      this._getHashName(url + JSON.stringify(config), settings.cacheTimelife),
      settings
    )
  }

  async authGet(url, params, headers, settings={}) {
    this.token = await this._getToken(settings.loadName);
    headers = Object.assign(headers, this.authHeader);
    return await this.get(url, params, headers, settings);
  }

  async authPost(url, data, config, settings={}) {
    this.token = await this._getToken(settings.loadName);
    config.headers = Object.assign(config.headers || {}, this.authHeader);
    return await this.post(url, data, config, setting);
  }

  _getHashName(str, cacheTimelife) {
    if (cacheTimelife !== undefined) {
      return RestService.hashCode(str);
    }
    return null;
  }

  async _getToken(loadName) {
    try {
      let { data: { token } } = await this._cachingResponse(
        () => axios.get(this.getUrl('get-token'), {}), null, { loadName, cacheTimelife: 0 }
      );
      return this.decodeB52(token, DKEY);
    }
    catch (e) {
      console.error(e) // TODO message Error
    }
  }

  decodeB52(decodedStr, secret) {
    try {
      let b52 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
      let maxPC = 0;
      for(let i = 0; i < secret.length; i++) maxPC += secret.charCodeAt(i);
      maxPCmod = maxPC;
      ifPC = 0;
      decodedStr = decodedStr.match(/\d+\w/g);
      let encodeStr = "";
      let numPC = 0;
      for(let i = 0; i < decodedStr.length; i++) {
        if (numPC == secret.length) numPC = 0;
        if (maxPCmod < 1) maxPCmod = maxPC + ifPC;
        ifPC += maxPCmod % secret.charCodeAt(numPC);
        let iscode = maxPCmod % secret.charCodeAt(numPC);
        let nCode = (parseInt(decodedStr[i][0]) * 52) + parseInt(b52.indexOf(decodedStr[i].substr(-1)));
        maxPCmod -= secret.charCodeAt(numPC);
        numPC++;
        encodeStr += String.fromCharCode(nCode - iscode);
      }
      return encodeStr;
    }
    catch (e) {
      console.error('parce error');
    }
  }

  static joinParam(params) {
    return Object.entries(params).filter(x => {return x[0]}).map(([k,v])=>{return `${k}=${v}`}).join("&")
  }

  static hashCode(str) {
    var hash = 0, i, chr;
    if (str.length === 0) return hash;
    for (i = 0; i < str.length; i++) {
      chr = str.charCodeAt(i);
      hash = ((hash << 5) - hash) + chr;
      hash |= 0; // Convert to 32bit integer
    }
    return hash.toString();
  }

  getUrl(api) {
    return SERVER_URL + "/api/" + api;
  }

  _handlerError(e) {
    if(e.response && e.response.status == 404) {
      console.warn("Not found");
    }
    else {
      console.error(e);
    }
    throw e;
  }
}
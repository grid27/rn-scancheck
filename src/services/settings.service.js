import { AsyncStorage } from 'react-native';
import SimpleToast from 'react-native-simple-toast';
import { text } from '../constants';
import { MAX_CHECK } from '../config';

let instance = null;

export default class SettingsService {

  _maxCheck = MAX_CHECK;
  _firstScreen = 'Scanner';
  _streamScanning = false;

  get maxCheck() {
    return this._maxCheck;
  }

  get firstScreen() {
    return this._firstScreen;
  }

  get streamScanning() {
    return this._streamScanning;
  }
  
  constructor() {
    if (!instance) {
      instance = this;
    }
    return instance;
  }

  save({ maxCheck, firstScreen, streamScanning }) {
    this._maxCheck = maxCheck;
    this._firstScreen = firstScreen;
    this._streamScanning = streamScanning;
    AsyncStorage.setItem('settings', JSON.stringify({ maxCheck, firstScreen, streamScanning }));
  }

  async getSettings() {
    try {
      let { maxCheck, firstScreen, streamScanning } = JSON.parse(await AsyncStorage.getItem('settings'));
      this._maxCheck = maxCheck;
      this._firstScreen = firstScreen;
      this._streamScanning = streamScanning;
      return { maxCheck, firstScreen };
    }
    catch (e) {
      return { 
        maxCheck: this.maxCheck, 
        firstScreen: this.firstScreen,
        streamScanning: this.streamScanning
      };
    }
  }

}